/**
 * @author Andrew McGillivary
 * This is a small program to produce a random 410 word text document inspired by 
 * Jorge Luis Borges' story 'The Library of Babel'.
 */

import java.lang.*;
import java.io.*;

public class LibraryofBabel {
    
    public static void main(String[] args) {
        String output_filename;
        
        // The total amount of words in the output. 410 is a reference to the short story wherein esach book has 410 pages.
        int total_words = 410;
        
        // Checking for arguments and carrying on accordingly.
        if (args.length > 1) {
            print_usage();
            return;
        }
        
        // The first argument specifies output_filename. It defaults to    LibraryofBabelOutput.
        if (args.length == 1) {
            output_filename = args[0];
        }
        else {
            // The default filename.
            output_filename = "LibraryofBabelOutput";
        }
        
        // A placeholder for when I need to capitalize chars.
        char capital_char;
        
        // Puts the entire alphabet including some punctuation into an array.
        char[] lexicon = "abcdefghijklmnopqrstuvwxyz',.?!".toCharArray();
        char previous = ' ';
        int word_count = 1;
        
        // A flag to determine whether the next letter needs to be capitalized.
        int capital_flag = 0;
        
        // How the output will be assembled.
        StringBuilder output = new StringBuilder();
        
        // Making sure to start it off with an uppercase letter and not a space or punctuation.
        int rand = (int)(Math.random()*25);
        char first_char = Character.toUpperCase(lexicon[rand]);
        output.append(first_char);
        
        while (word_count < total_words) {
            // The extending of the range by five allows for spaces to come about once every six letters.
            rand = (int)(Math.random()*(lexicon.length+5));
            
            // Making sure to put spaces and capitalization after punctuation. 
            if (previous == '.' || previous == '?' || previous == '!') {
                capital_flag = 1;
                output.append(" ");
                previous = ' ';
            }
            else if (rand >= lexicon.length || previous == ',') {
                if (previous == ' ') {
                    previous = ' ';
                }
                else {
                    output.append(' ');
                    previous = ' ';
                    word_count++;
                }
            }
            else if (previous == ' ') {
                rand = (int)(Math.random()*25);
                if (capital_flag == 1) {
                    capital_char = Character.toUpperCase(lexicon[rand]);
                    output.append(capital_char);
                    capital_flag = 0;
                    previous = lexicon[rand];
                }
                else {
                    output.append(lexicon[rand]);
                    previous = lexicon[rand];
                }
            }
            else {
                if (capital_flag == 1) {
                    capital_char = Character.toUpperCase(lexicon[rand]);
                    output.append(capital_char);
                    capital_flag = 0;
                    previous = lexicon[rand];
                }
                else {
                    output.append(lexicon[rand]);
                    previous = lexicon[rand];
                }
            }
        }
        
        String lib_text = output.toString();
        
        try {
            BufferedWriter output_file = new BufferedWriter(new FileWriter(output_filename + ".txt"));
            output_file.write(lib_text);
            output_file.close();
            System.out.println("File " + output_filename + ".txt has been saved to the current directory.");
        }
        catch (IOException e) {
            System.out.println("Exception");
        }
        //System.out.println(output.toString());
    }
    
    public static void print_usage() {
        System.out.println("java LibraryofBabel [arg1] \n arg1: specifies the desired filename of the output file. If this argument is \n left blank the filename defaults to LibraryofBabelOutput. Do not append \".txt\"  to your arg1.");
    }
}