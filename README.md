@author - Andrew McGillivary    last updated 8/27/17

This is a small java app inspired by Jorge Luis Borges story 'The Library of Babel' from his book "Ficciones".  The story describes an endless library wherein the books contain every possible ordering of 25 characters.  For my project I decided to use the 26 letters from the English alphabet, along with the comma, period, apostrophe, question mark, and exclamation point for a total of 31 characters.  I am also using whitespace, but not counting it as a character.  I decided to have the size of the output file at 410 where a word is defined as a string of characters separated by a space.  This is in reference to the story where each book in the titular library has 410 pages.

The app first separates the characters into their own spot in an array with length 31.  Then it picks a random number up to 25 to ensure that the output file begins with a letter and not whitespace or punctuation.  The first character is then added to a StringBuilder.  Then the program picks a random number from 0 to 36 and adds the corresponding character to the StringBuilder.  If the random number is between 31 and 36, then a space is added and the word count is incremented.  A space is always added after punctuation with the exception of the apostrophe.

Finally the StringBuilder saves it's output into a String using the toString() method.  Then a BufferedWriter writes that String to a file named "LibraryofBabylon.txt" in the current directory.

Possible Improvements:
    - Allow the user to choose what they would like to name the output file.
        - Completed (8/27)
    - Allow the user to select the desired length of the output file.
        - Unsure of how to do this since arguments are Strings not ints (8/27)
    - Try to capitalize the letters after punctuation.
        - Done with this (8/27)
    - Make sure there isn't any punctuation after spaces.
        - Done with this, constricted it so only letters can appear after spaces (8/27)
    - Add functionality for command line args including a usage statement.
        - Completed (8/27)